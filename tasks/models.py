from django.db import models

# Create your models here.
class Clients(models.Model):
	mac = models.CharField(primary_key=True, max_length=50)
	name = models.CharField(max_length=20, null=True,default=None)

class Images(models.Model):
	imageName = models.CharField(max_length=20, null=False, default=None, unique=True)
	diskSize=models.IntegerField(default=0)
	compressedSize=models.IntegerField(default=0)
	osType=models.CharField(max_length=20,default="Linux")

class Tasks(models.Model):
	# client = models.ForeignKey(Clients,on_delete=models.CASCADE,unique=True)
	taskType = models.CharField(max_length=30, null=False, default=None)

class Deployment(models.Model):
	taskId = models.ForeignKey(Tasks,on_delete=models.CASCADE)
	imageName = models.ForeignKey(Images,on_delete=models.CASCADE)
	local_deployment_mode=models.CharField(max_length=10,default="Exist")

class Cloning(models.Model):
	taskId = models.ForeignKey(Tasks,on_delete=models.CASCADE)
	imageName = models.CharField(max_length=20, null=False, default=None)
	osType=models.CharField(max_length=20,default="Linux")

class Partitions(models.Model):
	cloningId = models.ForeignKey(Cloning,on_delete=models.CASCADE)
	partitionNumber = models.CharField(max_length=20, null=False, default=None)

class ClientTasks(models.Model):
	clientId=models.ForeignKey(Clients,on_delete=models.CASCADE,unique=True)
	taskId=models.ForeignKey(Tasks,on_delete=models.CASCADE)

class Multicast(models.Model):
	taskId=models.ForeignKey(Tasks,on_delete=models.CASCADE)
	imageName = models.ForeignKey(Images,on_delete=models.CASCADE)
	receivers_no=models.IntegerField()
	status=models.IntegerField(default=0)
	mcastAddr=models.CharField(max_length=20, default="239.0.0.x")
	port=models.IntegerField(default=0)

class ROSIMSettings(models.Model):
	tftp_server=models.CharField(max_length=20, default="192.168.56.100")
	storage_server_ip=models.CharField(max_length=20, default="192.168.56.100")
	storage_server_http_port=models.IntegerField(default=8000)
	storage_server_share=models.CharField(max_length=20, default="/nfsroot")
	web_server_ip=models.CharField(max_length=20, default="192.168.56.100")
	web_server_port=models.IntegerField(default=8080)
	debug=models.BooleanField(default=True)
	global_deployment_mode=models.CharField(max_length=10,default="Exist")
	dhcp_server=models.CharField(max_length=20, default="192.168.56.100")
	dhcp_ip_start=models.CharField(max_length=20, default="192.168.56.20")
	dhcp_ip_end=models.CharField(max_length=20, default="192.168.56.80")
	dhcp_server_type=models.CharField(max_length=10,default="Full")
	dhcp_ip_gateway=models.CharField(max_length=20, default="192.168.56.100")
	dhcp_ip_dns=models.CharField(max_length=20, default="192.168.56.100")
	dhcp_subnet_mask=models.CharField(max_length=20, default="255.255.255.0")
	