# Generated by Django 3.0.3 on 2020-04-18 05:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0006_auto_20200312_1007'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tasks',
            name='client',
        ),
        migrations.CreateModel(
            name='Multicasting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(default=0)),
                ('imageName', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasks.Images')),
                ('taskId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasks.Tasks')),
            ],
        ),
        migrations.CreateModel(
            name='ClientTasks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('clientId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasks.Clients')),
                ('taskId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasks.Tasks')),
            ],
        ),
    ]
