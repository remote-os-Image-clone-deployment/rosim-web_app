# Generated by Django 3.0.3 on 2020-05-15 08:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0017_auto_20200515_0514'),
    ]

    operations = [
        migrations.AddField(
            model_name='rosimsettings',
            name='dhcp_ip_dns',
            field=models.CharField(default='192.168.56.100', max_length=20),
        ),
        migrations.AddField(
            model_name='rosimsettings',
            name='dhcp_ip_end',
            field=models.CharField(default='192.168.56.80', max_length=20),
        ),
        migrations.AddField(
            model_name='rosimsettings',
            name='dhcp_ip_gateway',
            field=models.CharField(default='192.168.56.100', max_length=20),
        ),
        migrations.AddField(
            model_name='rosimsettings',
            name='dhcp_ip_start',
            field=models.CharField(default='192.168.56.20', max_length=20),
        ),
        migrations.AddField(
            model_name='rosimsettings',
            name='dhcp_type',
            field=models.CharField(default='Full', max_length=10),
        ),
    ]
