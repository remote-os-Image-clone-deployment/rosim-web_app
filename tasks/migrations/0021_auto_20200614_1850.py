# Generated by Django 3.0.5 on 2020-06-14 18:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0020_auto_20200607_1635'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clienttasks',
            name='clientId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasks.Clients', unique=True),
        ),
    ]
