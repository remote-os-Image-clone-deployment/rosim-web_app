from django.contrib import admin
from django.urls import path, include
from . import views


from django.conf.urls.static import  static
from django.conf import settings


urlpatterns = [
    path('', views.index),#
    path('register', views.registration),#
    path('imagedeployment',views.ImageDeployment),#
    path('multicast',views.multicast),
    # path('multicastdeployment1',views.MulticastDeployment1),
    path('multicastdeployment',views.MulticastDeployment),
    path('cloning',views.cloning),#
    path('settings',views.displaySettings),
    path('getsettings',views.getSettings),
    path('createimage',views.createImage),
    path('updatesettings',views.updateSettings),
    path('tasks',views.getTasks),#
    path('taskstatus', views.taskStatus),#
    # path('clients', views.listClients),#
    path('clients', views.multicast),#
    path('settings', views.getSettings),#
    path('<str:mac>/',views.clientDetails),#

    ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)