from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib import auth
from tasks.models import Clients, Tasks, Images, Deployment, Cloning, Partitions, ClientTasks, Multicast, ROSIMSettings
from django.contrib import messages
import requests

def index(request):
	print(request.user)
	print("Hitting Home Page Successfull")
	#return JsonResponse(data)
	#return HttpResponse("Done and dusted")
	return render(request,'home.html',{'user':request.user})

def registration(request):
	name = "Unnamed"
	mac = request.POST.get('MAC')
	if request.POST.get('Name'):
		name = request.POST.get('Name')
	print(mac)
	clientquery=Clients.objects.filter(mac=mac)
	if not clientquery.exists():
		Clients.objects.create(mac=mac, name=name)
	
	return HttpResponse("Done and dusted")

def ImageDeployment(request):
	client = request.POST.get('client')
	print(client)
	imageName = request.POST.get('imageName')
	print(imageName)
	clientInstance = Clients.objects.get(mac=client)
	imageInstance = Images.objects.get(imageName=imageName)
	print(clientInstance.mac)
	taskInstance = Tasks.objects.create(taskType='UnicastDeployment')
	clienttasksInstance=ClientTasks.objects.create(clientId=clientInstance,taskId=taskInstance)
	Deployment.objects.create(taskId=taskInstance,imageName=imageInstance)
	messages.success(request, 'Your task was created successfully!') 
	return redirect('/tasks/'+client)
	#return HttpResponse("Done and dusted")

def cloning(request):
	client = request.POST.get('client')
	imageName = request.POST.get('partitionName')
	print(client)
	
	partitions = request.POST.getlist('partitions[]')
	print(partitions)	
	print(imageName)
	
	clientInstance = Clients.objects.get(mac=client)
	taskInstance = Tasks.objects.create(taskType='Cloning')
	clienttasksInstance=ClientTasks.objects.create(clientId=clientInstance,taskId=taskInstance)
	cloningInstance = Cloning.objects.create(taskId=taskInstance,imageName=imageName)
	for partition in partitions:
		Partitions.objects.create(cloningId=cloningInstance,partitionNumber=partition)
	messages.success(request, 'Your task was created successfully!') 
	return redirect('/tasks/'+client)

def multicast(request):
	multicastInstances = Clients.objects.all()
	listOfImages = Images.objects.all()
	return render(request,'multicast.html',{'multicastInstances':multicastInstances,'listOfImages':listOfImages})

# def MulticastDeployment1(request):
# 	Mclients= request.POST.getlist('Mclients[]')
# 	print(Mclients)
# 	imageName = request.POST.get('imageName')
# 	print(imageName)
# 	return HttpResponse("Done and dusted")
def MulticastDeployment(request):
	# client = request.POST.get('client')
	Mclients= request.POST.getlist('Mclients[]')
	print(Mclients)
	imageName = request.POST.get('imageName')
	print(imageName)
	di={}
	di['imageName']=imageName
	di['receivers']=len(Mclients)
	imageInstance = Images.objects.get(imageName=imageName)
	# print(clientInstance.mac)
	taskInstance = Tasks.objects.create(taskType='MulticastDeployment')
	di['taskId']=taskInstance.id
	for Mclient in Mclients: 
		clientInstance = Clients.objects.get(mac=Mclient)
		ClientTasks.objects.create(clientId=clientInstance,taskId=taskInstance)

	multicastInstance=Multicast.objects.create(taskId=taskInstance,imageName=imageInstance,receivers_no=len(Mclients))
	#create multicast session
	storage_server_http=ROSIMSettings.objects.get(id=1).storage_server_ip + ":"+ str(ROSIMSettings.objects.get(id=1).storage_server_http_port)
	url="http://"+storage_server_http+"/multicast/createSession"
	# "http://maps.googleapis.com/maps/api/geocode/json"
	print(url)
	r=requests.post(url,data=di)
	print(r)
	if r.status_code==200:
		data=r.json()
		multicastInstance.port=data['port']
		multicastInstance.mcastAddr=data['ip']
		multicastInstance.status=1
		multicastInstance.save()
	
	messages.success(request, 'Your task was created successfully!') 
	return redirect('/tasks/multicast')

def getTasks(request):
	mac = request.POST.get('MAC')
	taskquery=ClientTasks.objects.filter(clientId=mac)
	if not taskquery.exists():
		return HttpResponse("no task exist")
	task=taskquery[0].taskId

	# task = Tasks.objects.get(client=mac)
	print(task)
	
	di = {}
	di['id'] = task.id
	di['taskType'] = task.taskType

	if di['taskType'] == 'Cloning':
		cloningInstance = Cloning.objects.get(taskId=task)
		print(cloningInstance)
		di['imageName'] = cloningInstance.imageName
		di['osType']=cloningInstance.osType
		partitions = Partitions.objects.all().filter(cloningId=cloningInstance)
		print(partitions)
		listOfPartitions = []
		for i in partitions:
			listOfPartitions.append(i.partitionNumber)
		print(listOfPartitions)
		di['partitionNumber'] = listOfPartitions
		
	elif di['taskType'] == 'UnicastDeployment':
		deploymentInstance = Deployment.objects.get(taskId=task)
		print(deploymentInstance.imageName.imageName)
		di['imageName'] = deploymentInstance.imageName.imageName

	elif di['taskType']== 'MulticastDeployment':
		multicastInstance=Multicast.objects.get(taskId=task)
		di['imageName']=multicastInstance.imageName.imageName
		storage_server_http=ROSIMSettings.objects.get(id=1).storage_server_ip + ":"+ str(ROSIMSettings.objects.get(id=1).storage_server_http_port)
		url="http://"+storage_server_http+"/multicast/startSession"
		print(url)
		di2={}
		di2['taskId']=multicastInstance.taskId.id
		if multicastInstance.status==1:
			r=requests.post(url,data=di2)
			if r.status_code==200:
				data=r.json()
				if data['status']=='started' or data['status']=='already started please join':
					multicastInstance.status=2
					multicastInstance.save()
				else :
					print(data.status)
		di['port']=multicastInstance.port
			#start the multicast session if status=1
	print(di)

	return JsonResponse(di)


def taskStatus(request):
	taskId = request.POST.get('taskId')
	# taskId=int(taskId)
	print(taskId)
	status = request.POST.get('status')
	print(status)
	if status == '0':
		task = Tasks.objects.get(id=taskId)
		# if task.taskType == 'Cloning':
		# 	cloningInstance = Cloning.objects.get(taskId=task)
		# 	Images.objects.create(imageName=cloningInstance.partitionName)
		task.delete()

	return HttpResponse("Task status updated")

def listClients(request):
	clientObjects = Clients.objects.all()
	# listOfImages = Images.objects.all()
	#return HttpResponse("List of Clients")
	return render(request,'clients.html',{'clientlist':clientObjects})

def clientDetails(request,mac):
	clientInstance = Clients.objects.get(mac=mac)
	listOfImages = Images.objects.all()
	print(clientInstance)
	return render(request,'clientdetail.html',{'client':clientInstance, 'imagelist':listOfImages })

def displaySettings(request):
	settingsquery=ROSIMSettings.objects.filter(id=1) 
	if not settingsquery.exists(): 
		ROSIMSettings.objects.create() 				 #creates settings object with default values when first opened and then the user accesses the page
	settingsInstance=ROSIMSettings.objects.get(id=1)
	settings_di={}	
	print(settingsInstance.debug)
	print(type(settingsInstance.debug))
	if settingsInstance.debug:
		settings_di['debug']="True"
	else:
		settings_di['debug']="False"
	settings_di['tftp_server']=settingsInstance.tftp_server
	settings_di['storage_server_ip']=settingsInstance.storage_server_ip 
	settings_di['storage_server_share'] = settingsInstance.storage_server_share
	settings_di['storage_server_http_port'] = settingsInstance.storage_server_http_port
	settings_di['web_server_ip']=settingsInstance.web_server_ip 
	settings_di['web_server_port'] = str(settingsInstance.web_server_port)
	settings_di['dhcp_server']=settingsInstance.dhcp_server
	settings_di['dhcp_server_type']=settingsInstance.dhcp_server_type
	settings_di['dhcp_subnet_mask']=settingsInstance.dhcp_subnet_mask
	settings_di['dhcp_ip_start']=settingsInstance.dhcp_ip_start
	settings_di['dhcp_ip_end']=settingsInstance.dhcp_ip_end
	settings_di['dhcp_ip_gateway']=settingsInstance.dhcp_ip_gateway
	settings_di['dhcp_ip_dns']=settingsInstance.dhcp_ip_dns
	settings_di['global_deployment_mode']=settingsInstance.global_deployment_mode
	print(settings_di)
	# return JsonResponse(settings_di)
	return render(request,'settings.html',{'settings': settings_di})


def getSettings(request):
	settingsquery=ROSIMSettings.objects.filter(id=1) 
	if not settingsquery.exists(): 
		ROSIMSettings.objects.create() 				 #creates settings object with default values when first opened and then the user accesses the page
	settingsInstance=ROSIMSettings.objects.get(id=1)
	settings_di={}	

	settings_di['tftp_server']=settingsInstance.tftp_server
	settings_di['storage_server_ip']=settingsInstance.storage_server_ip 
	settings_di['storage_server_share'] = settingsInstance.storage_server_share
	settings_di['storage_server_http_port'] = settingsInstance.storage_server_http_port
	settings_di['web_server_ip']=settingsInstance.web_server_ip 
	settings_di['web_server_port'] = str(settingsInstance.web_server_port)
	settings_di['debug']=settingsInstance.debug
	settings_di['dhcp_server']=settingsInstance.dhcp_server
	settings_di['dhcp_server_type']=settingsInstance.dhcp_server_type
	settings_di['dhcp_subnet_mask']=settingsInstance.dhcp_subnet_mask
	settings_di['dhcp_ip_start']=settingsInstance.dhcp_ip_start
	settings_di['dhcp_ip_end']=settingsInstance.dhcp_ip_end
	settings_di['dhcp_ip_gateway']=settingsInstance.dhcp_ip_gateway
	settings_di['dhcp_ip_dns']=settingsInstance.dhcp_ip_dns
	settings_di['global_deployment_mode']=settingsInstance.global_deployment_mode
	print(settings_di)
	return JsonResponse(settings_di)
	# return render(request,'settings.html',{'settings': settings_di})

def createImage(request):
	Images.objects.create(imageName=request.POST.get('imageName'),diskSize=request.POST.get('diskSize'),compressedSize=request.POST.get('compressedSize'), osType=request.POST.get('osType'))
	return HttpResponse("Done and dusted")

#to create custom settings throuch web interface form 
def updateSettings(request): 
	settingsInstance=ROSIMSettings.objects.get(id=1)
	# print(request)
	print(type(request))
	settingsInstance.storage_server_ip=request.POST.get('storage_server_ip')
	settingsInstance.storage_server_http_port=request.POST.get('storage_server_http_port')
	settingsInstance.tftp_server=request.POST.get('tftp_server')
	settingsInstance.web_server_ip=request.POST.get('web_server_ip')
	settingsInstance.web_server_port=request.POST.get('web_server_port')
	settingsInstance.dhcp_server=request.POST.get('dhcp_server')
	settingsInstance.debug=request.POST.get('debug')
	settingsInstance.global_deployment_mode=request.POST.get('global_deployment_mode')
	settingsInstance.storage_server_share=request.POST.get('storage_server_share')
	settingsInstance.dhcp_subnet_mask=request.POST.get('dhcp_subnet_mask')
	settingsInstance.dhcp_server_type=request.POST.get('dhcp_server_type')
	settingsInstance.dhcp_ip_start=request.POST.get('dhcp_ip_start')
	settingsInstance.dhcp_ip_end=request.POST.get('dhcp_ip_end')
	settingsInstance.dhcp_ip_gateway=request.POST.get('dhcp_ip_gateway')
	settingsInstance.dhcp_ip_dns=request.POST.get('dhcp_ip_dns')
	
	settingsInstance.save()
	messages.success(request, 'Settings updated') 
	return redirect('/tasks/settings')